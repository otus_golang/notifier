package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/streadway/amqp"
	"gitlab.com/otus_golang/notifier/config"
	"gitlab.com/otus_golang/notifier/logger"
	"gitlab.com/otus_golang/notifier/queue"
	"go.uber.org/zap"
	"log"
)

var c *config.Config
var l *zap.Logger
var amqpQueue *amqp.Connection

func init() {
	initConfig()
	initLogger(c)
	initQueue(c)
}

func initConfig() {
	var err error

	c, err = config.GetConfig()

	if err != nil {
		log.Fatalf("unable to load config: %v", err)
	}
}

func initLogger(c *config.Config) {
	var err error
	l, err = logger.GetLogger(c)

	if err != nil {
		log.Fatalf("unable to load logger: %v", err)
	}
}

func initQueue(c *config.Config) {
	amqpQueue = queue.GetConnection(c)
}

var rootCmd = &cobra.Command{
	Use:   "notifier",
	Short: "notifier service",
	Long:  `notifier service`,
	Run: func(cmd *cobra.Command, args []string) {
		defer amqpQueue.Close()

		ch, err := amqpQueue.Channel()
		failOnError(err, "Failed to open a channel")
		defer ch.Close()

		err = ch.ExchangeDeclare(
			c.Queue["exchange"], // name
			"fanout",            // type
			true,                // durable
			false,               // auto-deleted
			false,               // internal
			false,               // no-wait
			nil,                 // arguments
		)
		failOnError(err, "Failed to declare an exchange")

		q, err := ch.QueueDeclare(
			c.Queue["queue"], // name
			true,             // durable
			false,            // delete when unused
			false,            // exclusive
			false,            // no-wait
			nil,              // arguments
		)
		failOnError(err, "Failed to declare a queue")

		err = ch.QueueBind(
			q.Name,     // queue name
			"",         // routing key
			"calendar", // exchange
			false,
			nil)
		failOnError(err, "Failed to bind a queue")

		msgs, err := ch.Consume(
			q.Name, // queue
			"",     // consumer
			true,   // auto-ack
			false,  // exclusive
			false,  // no-local
			false,  // no-wait
			nil,    // args
		)
		failOnError(err, "Failed to register a consumer")

		forever := make(chan bool)

		go func() {
			for d := range msgs {
				log.Printf(" [x] %s", d.Body)
			}
		}()

		log.Printf(" [*] Waiting for logs. To exit press CTRL+C")
		<-forever
	},
}

func failOnError(e error, s string) {
	if e != nil {
		l.Fatal(fmt.Sprintf("%s: %v", s, e))
	}
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatalf("root execute error: %v", err)
	}
}
