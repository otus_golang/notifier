package queue

import (
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/otus_golang/notifier/config"
	"log"
)

func GetConnection(c *config.Config) *amqp.Connection {
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/",
		c.Queue["user"],
		c.Queue["pass"],
		c.Queue["host"],
		c.Queue["port"],
	))

	if err != nil {
		log.Fatalf("unable to get queue config: %v", err)
	}

	return conn
}
