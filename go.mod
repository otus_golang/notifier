module gitlab.com/otus_golang/notifier

go 1.13

require (
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/streadway/amqp v0.0.0-20190827072141-edfb9018d271
	go.uber.org/zap v1.10.0
)
