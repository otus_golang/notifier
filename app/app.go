package app

import (
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/otus_golang/notifier/config"
	"go.uber.org/zap"
)

type App struct {
	Config *config.Config
	Logger *zap.Logger
}

func (a *App) Run() {

	fmt.Print(viper.AllSettings())
	fmt.Print(a.Config)
	fmt.Print("Hello, world!\n")
}
