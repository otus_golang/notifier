package main

import (
	"gitlab.com/otus_golang/notifier/cmd"
)

func main() {
	cmd.Execute()
}
